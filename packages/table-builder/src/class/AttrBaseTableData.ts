// 数据项
export class DataItem {
    // 对象key
    key: string;
    // 字段属性标识
    field: string;
    // 字段名称,
    name: string;
    // 允许输入字数
    wordsNumber: number | 255;
    // 输入框类型
    inputType: string;
    // 数据字段类型（static 静态属性，dynamic 动态属性[由用户自定义配置]）
    fieldType: string;
    // 提示文本
    placeholder: string;
    // 配置对象
    config: Object;
    constructor() {
        this.wordsNumber = 255;
        this.key = '';
        this.placeholder = '';
        this.field = '';
        this.name = '';
        this.inputType = '';
        this.fieldType = '';
        this.config = {}
    }
}

/**
 * 枚举：输入框类型
 */
export const InputType = [
    { value: 'input', title: '单行文本输入框' },
    { value: 'textarea', title: '多行文本输入框' },
    { value: 'inputNumber', title: '数字输入框' },
    { value: 'date', title: '日期选择器' },
    { value: 'daterange', title: '日期范围选择器' },
    { value: 'time', title: '时间选择器' },
    { value: 'timerange', title: '时间范围选择器' },
    { value: 'datetime', title: '日期时间选择器' },
    { value: 'datetimerange', title: '日期时间范围选择器' },
    { value: 'select', title: '选择器' },
    { value: 'multipleSelect', title: '多选选择器' }
];
export const InputTypeMap: any = {};
InputType.forEach(item => {
    InputTypeMap[item.value] = item;
});



// 表格数据对象
export class TableData {
    // 开启数据域配置
    keyCache: any;
    tableList: Array<string>;
    constructor() {
        this.keyCache = {};
        this.tableList = [];
    }
}
