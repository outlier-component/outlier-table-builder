import { randId } from "../utils";

// 表格对象
export class Table {
    __config: TableConfig;
    title: string;
    titleConfig: Object;
    bindData: string | null;
    children: Array<Tr>;
    constructor() {
        this.__config = new TableConfig();
        const tr = new Array<Tr>();
        tr.push(new Tr());
        this.children = tr;
        this.bindData = null;
        this.title = '';
        this.titleConfig = {};
    }
    // 设置拆分行标识
    setCToChildrenTable() {
        this.__config.c = 'childrenRow';
    }
    // 设置数据域表格标识
    setCToDatasetTable() {
        this.__config.c = 'dataset';
    }
}

// 行对象
export class Tr {
    __config: TrConfig;
    children: Array<Td>;
    constructor() {
        this.__config = new TrConfig();
        const td = new Array<Td>();
        td.push(new Td());
        this.children = td;
    }
}

// 行对象
export class Td {
    __config: TdConfig;
    children: Array<Table> | null;
    style: Object;
    // 显示文本
    text: string;
    // 绑定文本
    bindData: string;
    // 数据域绑定时该字段属于哪个数据域
    bindDataset: string;
    // 文本配置显示方式
    textConfig: string;
    constructor() {
        this.__config = new TdConfig();
        this.children = null;
        this.text = '';
        this.textConfig = 'text';
        this.bindData = '';
        this.bindDataset = '';
        this.style = {
            fontWeight: '',
            minHeight: 20,
            alignItems: 'center',
            justifyContent: 'flex-start'
        };
    }
}

class BaseConfig {
    // 节点类型
    c: string;
    // 唯一标识，主要用作删除节点时使用
    key: string;
    constructor(c: string) {
        this.c = c;
        this.key = randId();
    }
}

// 表格配置对象
export class TableConfig extends BaseConfig{
    constructor() {
        super('table');
    }
}

// 行配置对象
export class TrConfig extends BaseConfig{
    constructor() {
        super('tr');
    }
}

// 列配置对象
export class TdConfig extends BaseConfig {
    width: number | null;
    borderType: string | null;
    constructor() {
        super('td');
        this.width = 0;
        this.borderType = null;
    }
}
