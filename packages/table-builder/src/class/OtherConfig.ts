export interface Dialog {
    // 显示与隐藏
    show: boolean,
    // 标题
    title: string
}

export class TreeNodeInfo {
    index: number;
    children: Array<any>;
    constructor(index: number, children: Array<any>) {
        this.index = index;
        this.children = children;
    }
}

export class PreviewData {
    data: any;
    dataset: any;
    constructor() {
        this.data = {};
        this.dataset = {};
    }
}

export class FormPc {
    base: Array<Object>;
    dataset: Array<Object>;
    datasetData: Object;
    formConfig: Object;
    constructor() {
        this.base = [];
        this.dataset = [];
        this.datasetData = {};
        this.formConfig = {};
    }
}
