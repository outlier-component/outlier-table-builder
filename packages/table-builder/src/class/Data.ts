import AttrBaseConfigForm from './AttrBaseConfigForm'
import { TableData as AttrBaseTableData } from "./AttrBaseTableData";
import { Table } from "./table";
import { FormPc } from "./OtherConfig";

export default class Data {
    // 基础配置
    attrBaseConfigForm: AttrBaseConfigForm;
    // 表格基础数据属性配置对象
    attrBaseTableData: AttrBaseTableData;
    // 表格数据集属性配置对象
    attrBaseTableDataSet: AttrBaseTableData;
    // 表格数据集缓存的属性配置对象
    attrBaseTableDataSetData: Object;
    // 绘制的表格配置对象
    table: Array<Table>;
    // PC端表单
    formPC: FormPc;

    constructor() {
        this.attrBaseConfigForm = new AttrBaseConfigForm();
        this.attrBaseTableData = new AttrBaseTableData();
        this.attrBaseTableDataSet = new AttrBaseTableData();
        this.attrBaseTableDataSetData = {};
        this.formPC = new FormPc();
        this.table = new Array<Table>();
    }
    public static init(data: Data) {
        data.attrBaseConfigForm = new AttrBaseConfigForm();
        data.attrBaseTableData = new AttrBaseTableData();
        data.attrBaseTableDataSet = new AttrBaseTableData();
        data.attrBaseTableDataSetData = {};
        data.formPC = new FormPc();
        data.table = new Array<Table>();
    }
}
