import {TreeNodeInfo} from "../class/OtherConfig";

export function rand(min: number, max: number) {
    return Math.floor(Math.random() * (max - min)) + min;
}

// 获取一个时间ID
export function randId() {
    return `${new Date().getTime()}_${rand(1000, 9999)}`
}

const StringIsNumber = (value: any) => isNaN(Number(value)) === false;

/**
 * 枚举转数组
 * @param enums 枚举对象
 */
// @ts-ignore
export function enumToArray(enums: any) : Array {
    return Object.keys(enums)
        .filter(StringIsNumber)
        .map(key => enums[key]);
}

/**
 * 获取树节点所在索引
 * @param node 节点
 */
export function getTreeNodeIndex(node: any) : TreeNodeInfo{
    const data = node.data;
    const parent = node.parent;
    const children = parent.data.children || parent.data;
    return new TreeNodeInfo(
        children.findIndex((d: any) => d.__config.key === data.__config.key),
        children,
    );
}

/**
 * 向上节点查找数据域配置对象
 * @param node 当前节点
 */
export function findNodeDataset(node: any) : any{
    const parent = node.parent;
    if (parent) {
        const parentData = parent.data;
        if ((parentData.__config || {}).c === 'dataset') {
            return parent;
        } else {
            return findNodeDataset(parent);
        }
    }
    return null;
}

/**
 * 重写元素数组中的所有KEY
 * @param children 数组元素
 */
export function renderKey(children: Array<any>) {
    children.forEach(item => {
        item.__config.key = randId();
        if (item.children && item.children.length > 0) {
            renderKey(item.children);
        }
    })
}

// 检测对象是否为指定类型
export function isType(item: Object, type: string) {
    return Object.prototype.toString.call(item).indexOf(type) > 0;
}
