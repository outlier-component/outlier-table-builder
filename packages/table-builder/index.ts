import { App } from 'vue'
import OETableBuilder from './src/index.vue'

// 定义 install 方法， App 作为参数
OETableBuilder.install = (app: App): void => {
    app.component(OETableBuilder.name, OETableBuilder);
};

export default OETableBuilder
