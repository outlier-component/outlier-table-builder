import { App } from 'vue'
import OETableBuilder from './table-builder'
import OETablePreview from './table-builder/src/component/tablePreview/index.vue'

// 所有组件列表
const components = [ OETableBuilder, OETablePreview ];

// 定义 install 方法， App 作为参数
const install = (app: App): void => {
    // 遍历注册所有组件
    components.map((component) => app.component(component.name, component))
};

export default {
    OETableBuilder,
    OETablePreview,
    install
}
