import { createApp } from 'vue'
import App from './App.vue'
import ElementPlus from 'element-plus';
import 'element-plus/theme-chalk/index.css';

import OutlierElement from '../packages/index';

const app = createApp(App);
app.use(ElementPlus);
app.use(OutlierElement);
app.mount('#app');
