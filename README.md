# outlier-table-builder 复杂填报表格构造器

<h3>V0.0.5 开发测试版</h3>

## Project setup
```
npm install outlier-table-builder
```

```js
import OutlierElement from 'outlier-table-builder';
import 'outlier-table-builder/lib/index.css'

Vue.use(OutlierElement)
```

## Demo样例

https://outlier-component.gitee.io/outlier-table-builder